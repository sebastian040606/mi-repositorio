#include <iostream>
#include <Windows.h>
#include <conio.h>
#include <string>
#include <ctime>

using namespace std;

void motoxy(int x, int y) {
    COORD coord;
    coord.X = x;
    coord.Y = y;
    SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}
int n_rand(int min, int max) {
    return rand() % (max - min + 1) + min;
}
enum Colors {
    BLACK,
    DARK_RED,
    DARK_GREEN,
    DARK_YELLOW,
    DARK_BLUE,
    DARK_MAGENTA,
    DARK_CYAN,
    DARK_WHITE,
    BRIGHT_BLACK,
    BRIGHT_RED,
    BRIGHT_GREEN,
    BRIGHT_YELLOW,
    BRIGHT_BLUE,
    BRIGHT_MAGENTA,
    BRIGHT_CYAN,
    WHITE
};
void hideCursor() {
    HANDLE hCon;
    hCon = GetStdHandle(STD_OUTPUT_HANDLE);
    CONSOLE_CURSOR_INFO cci;
    cci.dwSize = 2;
    cci.bVisible = FALSE;
    SetConsoleCursorInfo(hCon, &cci);
}
int _WINCOLOR_[] = { 0, 4, 2, 6, 1, 5, 3, 7, 8, 12, 10, 14, 9, 13, 11, 15 };
void clearColor() {
    HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(handle, 7);
}
void color(int forecolor, int backcolor = BLACK) {
    forecolor = _WINCOLOR_[forecolor];
    backcolor = _WINCOLOR_[backcolor];
    HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(handle, backcolor << 4 | forecolor);
}

int main() {
    srand(time(0));
    int rpta = 0;
    int dificultad = 0;
    char ignore = 0;

    bool gameover = false;

    do {
        string dif; // dificultad en nombre
        int points = 0; // puntos obetenidos en tiempo real
        int p_lluvia = 0; // porcentaje de caida de lluvia
        do {
            motoxy(59, 1); cout << "x";
            motoxy(38, 3); color(1); cout << "Se�ale la dificultad en la que quiere jugar: ";
            motoxy(53, 4); color(8); cout << "1. pollo";
            motoxy(53, 5); color(8); cout << "2. normal (Recomendado)";
            motoxy(53, 6); color(8); cout << "3. dificil";
            motoxy(53, 7); color(8); cout << "4. insano"; clearColor();
            motoxy(55, 8); cout << ">> ";
            cin >> rpta;
            if (rpta <= 0 || rpta >= 5) {
                motoxy(35, 9);  color(DARK_RED);  cout << "POR FAVOR INGRESE EL NUMERO DENTRO DE LOS PARAMETROS";
                motoxy(58, 8); cout << "                     ";
            }
        } while (rpta <= 0 || rpta >= 5);
        motoxy(35, 9); cout << "                                                                                                                        ";
        do {
            motoxy(52, 9); color(1);  cout << "Config. avanzada";
            motoxy(46, 10); color(1); cout << "Porcentaje de caida de lluvia";
            motoxy(53, 11); color(8); cout << "1. pollo (2%)";
            motoxy(53, 12); color(8); cout << "2. default (5%)";
            motoxy(53, 13); color(8); cout << "3. dificil (10%)";
            motoxy(53, 14); color(8); cout << "4. insano(15%)"; clearColor();
            motoxy(55, 15); cout << ">> ";
            cin >> p_lluvia;
            if (p_lluvia > 4 || p_lluvia < 1) {
                motoxy(38, 16); color(DARK_RED); cout << "Diossss, por favor ingrese un numero correcto";
                motoxy(58, 15); cout << "             ";
            }
        } while (p_lluvia > 4 || p_lluvia < 1);
        switch (rpta) {
        case 1:
            dificultad = 50;
            dif = "pollo";
            break;
        case 2:
            dificultad = 40;
            dif = "normal";
            break;
        case 3:
            dificultad = 30;
            dif = "dificil";
            break;
        case 4:
            dificultad = 15;
            dif = "insano";
            break;
        }
        switch (p_lluvia) {
        case 1:
            p_lluvia = 2;
            break;
        case 2:
            p_lluvia = 5;
            break;
        case 3:
            p_lluvia = 10;
            break;
        case 4:
            p_lluvia = 15;
            break;
        }
        system("cls");
        hideCursor();
        const int gotas = 60;
        const int posicion = 1;

        int pos_y[gotas] = { 0 };
        int pos_x[posicion] = { 59 };
        for (int i = 0; i < gotas; ++i) {
            pos_y[i] = -1;
        }

        clearColor();

        for (int i = 0; i < gotas; ++i) {
            motoxy(29 + i, 1);
            color(n_rand(1, 15));
            cout << char(n_rand(65, 90));
            clearColor();
        }
        for (int j = 0; j <= 1; ++j) {
            for (int i = 0; i < 29; ++i) {
                color(4);
                motoxy(28 + (j * 62), i);
                cout << char(186);
            }
        }
        while (!gameover) {
            for (int i = 0; i < gotas; ++i) {
                if (pos_y[i] == -1 && n_rand(0, 100) < p_lluvia) {
                    pos_y[i] = 1;
                }
                if (pos_y[i] >= 1 && pos_y[i] < 29) {
                    motoxy(29 + i, pos_y[i]);
                    color(n_rand(1, 15));
                    cout << char(n_rand(65, 90));
                    clearColor();

                    motoxy(29 + i, pos_y[i] - 1);
                    cout << " ";
                    ++pos_y[i];
                }
                if (pos_y[i] == 29) {

                    motoxy(29 + i, 28);
                    cout << " ";
                    pos_y[i] = -1;
                }
                if (29 + i == pos_x[0] && pos_y[i] == 27) {

                    gameover = true;


                }
            }

            if (_kbhit()) {
                char tecla = _getch();
                motoxy(pos_x[0], 27);
                cout << " ";
                if (tecla == 'a' || tecla == 'A' && pos_x[0] > 28) {
                    --pos_x[0];
                }
                if (tecla == 'd' || tecla == 'D' && pos_x[0] < 90) {
                    ++pos_x[0];
                }
                motoxy(pos_x[0], 27);
                cout << (char)219;
            }
            Sleep(dificultad);
            ++points;
            motoxy(0, 0);
            cout << "puntos: " << points;
            motoxy(0, 1);
            cout << "modo " << dif;
            motoxy(0, 2);
            cout << "% de lluvia: " << p_lluvia;
        }
        system("cls");
        motoxy(57, 10); color(DARK_RED);  cout << "COLISSION";
        motoxy(52, 16); cout << "Puntos obtenidos: ";
        motoxy(70, 16);  color(2) << points;
        motoxy(47, 17); cout << "Quieres repetir partida?(S/N)";
        motoxy(57, 18); cout << ">> ";
        motoxy(60, 18);
        ignore = 0;
        cin >> ignore;
        if (ignore == 's' || ignore == 'S') {
            gameover = false;
            system("cls");
        }
    } while (ignore == 'S' || ignore == 's');


    return 0;
}
