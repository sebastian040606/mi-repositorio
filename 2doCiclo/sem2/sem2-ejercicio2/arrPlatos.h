#pragma once

#include "Plato.hpp"
#include <vector>

class arrPlato {
private:
	int numPlatos;
	vector<plato*>* arrPlatos;
public:
	arrPlato();
	~arrPlato();

	void agregarElemento();
	void agregarElementoAleatorio();
	void modificarElemento(int i);
	void eliminarElemento(int i);
	void imprimirElemento(int i);
	void imprimirTodo();
	void reportePicante();
	void reportePlatoCaro();

};

arrPlato::arrPlato() {
	this->numPlatos = 0;
	this->arrPlatos = new vector<plato*>();
}
arrPlato::~arrPlato() {
	delete[] arrPlatos;
}

void arrPlato::agregarElemento(){
	plato* obj = new plato();
	string aux_plato;
	bool aux_esFrio, aux_esPicante;
	float aux_cantCalorias, aux_precioProm;
	cout << "Ingrese nombre del plato: ";
	cin >> aux_plato;
	cout << "Ingrese si es caliente o frio(1: caliente, 0: frio): ";
	cin >> aux_esFrio;
	cout << "Ingrese si es picante(1: picante, 0: no picante): ";
	cin >> aux_esPicante;
	cout << "Ingrese cuantas calorias contiene: ";
	cin >> aux_cantCalorias;
	cout << "Ingrese cual es su precio promedio";
	cin >> aux_precioProm;
	obj->setNombre(aux_plato);
	obj->setEsFrio(aux_esFrio);
	obj->setEsPicante(aux_esPicante);
	obj->setCantCalorias(aux_cantCalorias);
	obj->setPrecioProm(aux_precioProm);
	arrPlatos->push_back(obj);
	this->numPlatos++;
	cout << "Plato agregado";
}
void arrPlato::agregarElementoAleatorio(){
	plato* obj = new plato();
	obj->generarPlatoAleatorio();
	arrPlatos->push_back(obj);
	cout << "Elemento aleatorio generado con exito";
	this->numPlatos++;
}
void arrPlato::modificarElemento(int i){
	string aux_plato;
	bool aux_esFrio, aux_esPicante;
	float aux_cantCalorias, aux_precioProm;
	cout << "El objeto a modificar es el siguiente: ";
	arrPlatos->at(i)->imprimirDatos();
	cout << "Ingrese nuevo nombre del plato: ";
	cin >> aux_plato;
	cout << "Ingrese si el plato es frio o caliente(1: frio, 2: caliente): ";
	cin >> aux_esFrio;
	cout << "Ingrese si el plato es picante o no(1: es picante, 2: no es picante): ";
	cin >> aux_esPicante;
	cout << "Ingrese nueva cantidad de calorias: ";
	cin >> aux_cantCalorias;
	cout << "Ingrese nuevo precio del plato: ";
	cin >> aux_precioProm;
	arrPlatos->at(i)->setNombre(aux_plato);
	arrPlatos->at(i)->setEsFrio(aux_esFrio);
	arrPlatos->at(i)->setEsPicante(aux_esPicante);
	arrPlatos->at(i)->setCantCalorias(aux_cantCalorias);
	arrPlatos->at(i)->setPrecioProm(aux_precioProm);
	cout << "El plato ha sido modificado con exito";
}
void arrPlato::eliminarElemento(int i){
	cout << "Esta eliminando el siguiente objeto: ";
	arrPlatos->at(i)->imprimirDatos();
	arrPlatos->erase(arrPlatos->begin() + i);
	cout << "El plato se ha eliminado correctamente";
	this->numPlatos--;
}
void arrPlato::imprimirElemento(int i){
	arrPlatos->at(i)->imprimirDatos();
}
void arrPlato::imprimirTodo(){
	for (int n = 0; n < arrPlatos->size(); ++n) {
		arrPlatos->at(n)->imprimirDatos();
    }
}
void arrPlato::reportePicante(){
	int count;
	for (int i = 0; i < arrPlatos->size(); ++i) {
		if (arrPlatos->at(i)->getEsPicante() == true) {
			arrPlatos->at(i)->imprimirDatos();
			++count;
		}
    }
	cout << "\nel numero de platos picantes es: " << count;
}
void arrPlato::reportePlatoCaro(){
	int count;
	for (int i = 0; i < arrPlatos->size(); ++i) {
		if (arrPlatos->at(i)->getPrecioProm() > 100) {
			arrPlatos->at(i)->imprimirDatos();
			count++;
		}
	}
	cout << "El numero de platos mayores de 100 soles son: " << count;

}