#include "arrPlatos.h"
#include <conio.h>

int main() {
	srand(time(0));
	arrPlato* VectordePlatos = new arrPlato();
	bool seguir = true;
	int op, index;

	while (seguir) {
		system("cls");
		cout << "Menu Principal" << endl;
		cout << "1. Agregar plato aleatorio" << endl;
		cout << "2. Agregar nuevo plato" << endl;
		cout << "3. Modificar plato" << endl;
		cout << "4. Elminar plato" << endl;
		cout << "5. Imprimir datos de un plato" << endl;
		cout << "6. Imprimir todos los platos" << endl;
		cout << "7. Reporte de platos picantes" << endl;
		cout << "8. Reportes de platos de mas de 100 soles" << endl;
		cout << "9. Salir" << endl;
		cout << ">> ";
		cin >> op;
		switch (op){
		case 1:
			system("cls");
			VectordePlatos->agregarElementoAleatorio();
			break;
		case 2:
			system("cls");
			VectordePlatos->agregarElemento();
			break;
		case 3:
			system("cls");
			cout << "Ingrese posicion de plato a modificar: ";
			cin >> index;
			VectordePlatos->modificarElemento(index - 1);
			break;
		case 4:
			system("cls");
			cout << "Ingrese posicion de plato a eliminar: ";
			cin >> index;
			VectordePlatos->eliminarElemento(index-1);
			break;
		case 5:
			system("cls");
			cout << "Ingrese posicion del plato a imprimir: ";
			cin >> index;
			VectordePlatos->imprimirElemento(index - 1);
			break;
		case 6:
			system("cls");
			VectordePlatos->imprimirTodo();
			break;
		case 7:
			system("cls");
			VectordePlatos->reportePicante();
			break;
		case 8:
			system("cls");
			VectordePlatos->reportePlatoCaro();
			break;
		case 9:
			system("cls");
			seguir = false;
			break;
		}
		_getch();
	}

	return 0;
}