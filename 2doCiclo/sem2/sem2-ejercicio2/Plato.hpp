#pragma once

#include <iostream>
#include <string>

using namespace std;

class plato {
private:
	string nombre;
	bool esFrio;
	bool esPicante;
	float cantCalorias;
	float PrecioProm;
public:
	plato();
	~plato();

	void setNombre(string nombre);
	void setEsFrio(bool esFrio);
	void setEsPicante(bool esPicante);
	void setCantCalorias(float cantCalorias);
	void setPrecioProm(float PrecioProm);

	string getNombre();
	bool getEsFrio();
	bool getEsPicante();
	float getCantCalorias();
	float getPrecioProm();

	void imprimirDatos();
	void generarPlatoAleatorio();
};

plato::plato() {
	this->nombre = "";
	this->esFrio = 0;
	this->esPicante = 0;
	this->cantCalorias = 0;
	this->PrecioProm = 0;
}
plato::~plato(){}

void plato::setNombre(string nombre) {
	this->nombre = nombre;
}
void plato::setEsFrio(bool esFrio) {
	this->esFrio = esFrio;
}
void plato::setEsPicante(bool esPicante) {
	this->esPicante = esPicante;
}
void plato::setCantCalorias(float cantCalorias) {
	this->cantCalorias = cantCalorias;
}
void plato::setPrecioProm(float PrecioProm) {
	this->PrecioProm = PrecioProm;
}

string plato::getNombre() {
	return this->nombre;
}
bool plato::getEsFrio() {
	return this->esFrio;
}
bool plato::getEsPicante() {
	return this->esPicante;
}
float plato::getCantCalorias() {
	return this->cantCalorias;
}
float plato::getPrecioProm() {
	return this->PrecioProm;
}

void plato::imprimirDatos() {
	cout << "\n\nDatos del plato: " << endl;
	cout << "nombre: " << this->nombre << endl;
	cout << "temperatura del plato: " << (this->esFrio == true ? "Frio" : "Caliente") << endl;
	cout << "Grado de picante: " << (this->esPicante == true ? "Picante" : "No Picante") << endl;
	cout << "Cantidad de calorias: " << this->cantCalorias << endl;
	cout << "Precio promedio: " << this->PrecioProm << endl;
}

void plato::generarPlatoAleatorio() {
	System::Random x;
	this->nombre = "plato Aleatorio";
	this->esFrio = x.Next(0, 2);
	this->esPicante = x.Next(0, 2);
	this->cantCalorias = x.Next(90, 600);
	this->PrecioProm = x.Next(60, 200);
}