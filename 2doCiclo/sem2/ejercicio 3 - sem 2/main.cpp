#include "CVectorContactos.h"
#include <conio.h>

void clear() {
	system("cls");
}

int main() {
	srand(time(nullptr));
	CVectorContactos* newVectorContacts = new CVectorContactos();
	bool seguir = true;
	int op, newop;
	char cnewop;
	while (seguir) {
		clear();
		cout << "Menu Pirncipal de contactos" << endl;
		cout << "1. Generar nuevo contacto aleatorio" << endl;
		cout << "2. Agregar nuevo contacto" << endl;
		
		cout << "3. Imprimir por indice" << endl;
		cout << "4. Imprimir todo" << endl;
		
		cout << "5. Elminar contacto" << endl;
		cout << "6. Modificar contacto" << endl;
		cout << "7. Reporte de contactos por numero de mes de nacimiento" << endl;
		cout << "8. Reporte de Contactos por sexo" << endl;
		cout << "9. Reporte de Contactos con preferencia de Red Social Whatsapp y FAcebook" << endl;
		cout << "10. Salir" << endl;
		cout << ">> ";
		cin >> op;
		switch (op){
		case 1:
			clear();
			newVectorContacts->agregarAleatorio();
			cout << "Nuevo contacto generado con exito";
			break;
		case 2:
			clear();
			newVectorContacts->agregarNuevoContacto();
			break;
		case 3:
			clear();
			cout << "Digite el numero de contacto a imprimir: ";
			cin >> op;
			newVectorContacts->imprimirXIndex(op-1);
			break;
		case 4:
			clear();
			newVectorContacts->imprimirTodo();
			break;
		case 5:
			clear();
			cout << "Ingrese numero de contacto a eliminar: ";
			cin >> newop;
			newVectorContacts->eliminarContacto(newop-1);
			break;
		case 6:
			clear();
			cout << "Ingrese numero de contacto a modificar: ";
			cin >> newop;
			newVectorContacts->modificarContacto(newop-1);
			break;
		case 7:
			clear();
			cout << "Ingrese numero de mes: ";
			cin >> newop;
			newVectorContacts->reporteContactosXMes(newop);
			break;
		case 8:
			clear();
			cout << "Ingrese sexo en mayuscula(M o F): ";
			cin >> cnewop;
			newVectorContacts->reporteContactosXSexo(cnewop);
			break;
		case 9:
			clear();
			newVectorContacts->reporteContactosRedSocialFacebookWhatsapp();
			break;
		case 10:
			clear();
			seguir = false;
			cout << "Presione enter para cerra programa...";
			break;
		}
		_getch();
	}


	return 0;
}