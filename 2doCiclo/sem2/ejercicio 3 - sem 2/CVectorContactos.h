#pragma once
#include "CContacto.h"
#include <vector>

class CVectorContactos {
private:
	int numContactos;
	vector<CContacto*>* ArrContacto;
public:
	CVectorContactos();
	~CVectorContactos();
	void agregarAleatorio();
	void agregarNuevoContacto();
	void imprimirXIndex(int index);
	void imprimirTodo();
	void eliminarContacto(int i);
	void modificarContacto(int i);
	void reporteContactosXMes(int numMes);
	void reporteContactosXSexo(char MayusSexo);
	void reporteContactosRedSocialFacebookWhatsapp();
};

CVectorContactos::CVectorContactos() {
	this->numContactos = 0;
	ArrContacto = new vector<CContacto*>();
}
CVectorContactos::~CVectorContactos() {
	delete[] ArrContacto;
}
void CVectorContactos::agregarAleatorio() {
	CContacto* newcontact = new CContacto();
	newcontact->CrearContactoAleatorio();
	ArrContacto->push_back(newcontact);
	numContactos++;
}
void CVectorContactos::agregarNuevoContacto() {
	CContacto* newcontact = new CContacto();
	string aux_nombre, aux_facultad, aux_email;
	int aux_telefono, aux_fecnac, aux_redsocial[5];
	char aux_sexo;
	cout << "\nNuevo Contacto" << endl;
	cout << "Ingrese nombre: "; cin >> aux_nombre;
	cout << "Ingrese telefono: "; cin >> aux_telefono;
	cout << "Ingrese sexo(M o F): "; cin >> aux_sexo;
	cout << "Ingrese facultad que pertenece (Empresas, Derecho, Administracion, Arquitectura, Salud. Educacion, Humanidades, Ingenieria): "; cin >> aux_facultad;
	cout << "Ingrese fecha de nacimiento(ejm: 17112005): "; cin >> aux_fecnac;
	cout << "Ingrese email: "; cin >> aux_email;

	cout << "Ingrese Red Social de Preferencia (0: No, 1: Si): " << endl;
	cout << "Usa Instagram: "; cin >> aux_redsocial[0];
	cout << "Usa Facebook: "; cin >> aux_redsocial[1];
	cout << "Usa Youtube: "; cin >> aux_redsocial[2];
	cout << "Usa Whatsapp: "; cin >> aux_redsocial[3];
	cout << "Usa Twitter: "; cin >> aux_redsocial[4];
	
	newcontact->setNombre(aux_nombre);
	newcontact->setTelefono(aux_telefono);
	newcontact->setSexo(aux_sexo);
	newcontact->setFacultad(aux_facultad);
	newcontact->setFecNac(aux_fecnac);
	newcontact->setEmail(aux_email);
	for (int i = 0; i < 5; ++i) {
		newcontact->setRedSocial(i, aux_redsocial[i]);
	}
	ArrContacto->push_back(newcontact);
	cout << "Nuevo Contacto agregado satisfactoriamente";
}

void CVectorContactos::imprimirXIndex(int index) {
	ArrContacto->at(index)->imprimirDatos();
}
void CVectorContactos::imprimirTodo() {
	for (int i = 0; i < ArrContacto->size(); ++i) {
		ArrContacto->at(i)->imprimirDatos();
	}
}

void CVectorContactos::eliminarContacto(int i) {
	ArrContacto->erase(ArrContacto->begin() + 1);
	cout << "Contacto eliminado con exito";
}
void CVectorContactos::modificarContacto(int i) {
	string aux_nombre, aux_facultad, aux_email;
	int aux_telefono, aux_fecnac, aux_redsocial[5];
	char aux_sexo;
	cout << "Contacto a modificar: ";
	ArrContacto->at(i)->imprimirDatos();
	cout << "\nModificando Contacto" << endl;
	cout << "Ingrese nombre: "; cin >> aux_nombre;
	cout << "Ingrese telefono: "; cin >> aux_telefono;
	cout << "Ingrese sexo(M o F): "; cin >> aux_sexo;
	cout << "Ingrese facultad que pertenece (Empresas, Derecho, Administracion, Arquitectura, Salud. Educacion, Humanidades, Ingenieria)"; cin >> aux_facultad;
	cout << "Ingrese fecha de nacimiento(ejm: 17112005): "; cin >> aux_fecnac;
	cout << "Ingrese email: "; cin >> aux_email;
	cout << "Ingrese Red Social de Preferencia (0: No, 1: Si): " << endl;
	cout << "Usa Instagram: "; cin >> aux_redsocial[0];
	cout << "Usa Facebook: "; cin >> aux_redsocial[1];
	cout << "Usa Youtube: "; cin >> aux_redsocial[2];
	cout << "Usa Whatsapp: "; cin >> aux_redsocial[3];
	cout << "Usa Twitter: "; cin >> aux_redsocial[4];

	ArrContacto->at(i)->setNombre(aux_nombre);
	ArrContacto->at(i)->setTelefono(aux_telefono);
	ArrContacto->at(i)->setSexo(aux_sexo);
	ArrContacto->at(i)->setFacultad(aux_facultad);
	ArrContacto->at(i)->setFecNac(aux_fecnac);
	ArrContacto->at(i)->setEmail(aux_email);
	for (int n = 0; n < 5; ++n) {
		ArrContacto->at(i)->setRedSocial(n, aux_redsocial[n]);
	}
	cout << "\nContacto modificado con exito";
}
void CVectorContactos::reporteContactosXMes(int numMes) {
	int aux_anio, aux_mes, aux_dia, aux_fecnac, count= 0;
	cout << "Contactos con cumpleanios en el mes " << numMes << endl;
	for (int n = 0; n < ArrContacto->size(); ++n) {
		aux_fecnac = ArrContacto->at(n)->getFecNac();
		aux_dia = aux_fecnac / 1000000;
		aux_anio = aux_fecnac % 10000;
		aux_mes = (aux_fecnac - aux_dia * 1000000 - aux_anio) / 10000;
		if (aux_mes == numMes) {
			ArrContacto->at(n)->imprimirDatos();
			++count;
		}
	}
	cout << "\n\nTienes " << count << " contactos con cumpleanios en el mes " << numMes;
}
void CVectorContactos::reporteContactosXSexo(char MayusSexo) {
	int count = 0;
	cout << "Reporte de contactos con sexo: " << MayusSexo << endl;
	for (int n = 0; n < ArrContacto->size(); ++n) {
		if (ArrContacto->at(n)->getSexo() == MayusSexo) {
			ArrContacto->at(n)->imprimirDatos();
			++count;
		}
	}
	cout << "\n\nTienes " << count << " contactos del sexo: " << MayusSexo;
}
void CVectorContactos::reporteContactosRedSocialFacebookWhatsapp() {
	int count = 0;
	for (int i = 0; i < ArrContacto->size(); ++i) {
		if (ArrContacto->at(i)->getRedSocial(0) == false && ArrContacto->at(i)->getRedSocial(1)==true && ArrContacto->at(i)->getRedSocial(2) == false && ArrContacto->at(i)->getRedSocial(3)==true && ArrContacto->at(i)->getRedSocial(4) == false) {
			ArrContacto->at(i)->imprimirDatos();
			++count;
		}
	}
	cout << "\n\nEn total hay " << count << " contactos con red social Whatsapp y Facebook";
}
