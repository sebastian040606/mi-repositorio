#pragma once

#include <iostream>
#include <string>

using namespace std;

class CContacto {
private:
	string nombre;
	int telefono;
	char sexo; // M o F
	string facultad;
	int fecNac;
	string email;
	bool redSocial[5]; //Instagram, facebook, youtube, whatsap, twiter, 
public:
	CContacto();
	~CContacto();
	void setNombre(string nombre);
	void setTelefono(int telefono);
	void setSexo(char sexo);
	void setFacultad(string facultad);
	void setFecNac(int fecnac);
	void setEmail(string email);
	void setRedSocial(int pos, bool redsocial);

	int getFecNac();
	char getSexo();
	bool getRedSocial(int pos);

	void imprimirDatos();
	void CrearContactoAleatorio();
};

CContacto::CContacto() {
	this->nombre = "";
	this->telefono = 0;
	this->sexo = ' ';
	this->facultad = "";
	this->fecNac = 0;
	this->email = "";
	this->redSocial[5] = 0;
}
CContacto::~CContacto() {}

void CContacto::setNombre(string nombre) {
	this->nombre = nombre;
}
void CContacto::setTelefono(int telefono) {
	this->telefono = telefono;
}
void CContacto::setSexo(char sexo) {
	this->sexo = sexo;
}
void CContacto::setFacultad(string facultad) {
	this->facultad = facultad;
}
void CContacto::setFecNac(int fecnac) {
	this->fecNac = fecnac;
}
void CContacto::setEmail(string email) {
	this->email = email;
}
void CContacto::setRedSocial(int pos, bool trueorfalse) {
	this->redSocial[pos] = trueorfalse;
}

int CContacto::getFecNac() {
	return this->fecNac;
}
char CContacto::getSexo() {
	return this->sexo;
}
bool CContacto::getRedSocial(int pos) {
	return this->redSocial[pos];
}



void CContacto::imprimirDatos() {
	string aux_redsocial;
	cout << "\n\nDatos del contacto: " << endl;
	cout << "Nombre: " << this->nombre << endl;
	cout << "Telefono: " << this->telefono << endl;
	cout << "Sexo: " << this->sexo << endl;
	cout << "Facultad: " << this->facultad << endl;
	cout << "Fecha de Nacimiento: " << this->fecNac << endl;
	cout << "Correo electronico: " << this->email << endl;
	cout << "Redes Sociales de preferencia: ";
	for (int i = 0; i < 5; ++i) {
		if (this->redSocial[i]) {
			switch (i){
			case 0:
				cout << "Instagram ";
				break;
			case 1:
				cout << "Facebook ";
				break;
			case 2:
				cout << "Youtube ";
				break;
			case 3:
				cout << "Whatsapp ";
				break;
			case 4:
				cout << "Twitter ";
				break;
			}
		}
	}
}

void CContacto::CrearContactoAleatorio() {
	System::Random x;
	int aux_facultad, aux_dia, aux_mes, aux_anio;
	this->nombre = "Nombrealeatorio";
	this->telefono = x.Next(10000000, 10000000000);
	this->sexo = (x.Next(0, 2) ? 'M' : 'F');
	aux_facultad = x.Next(0, 8);
	switch (aux_facultad){
	case 0:
		this->facultad = "Empresas";
		break;
	case 1:
		this->facultad = "Derecho";
		break;
	case 2:
		this->facultad = "Administracion";
		break;
	case 3:
		this->facultad = "Arquitectura";
		break;
	case 4:
		this->facultad = "Salud";
		break;
	case 5:
		this->facultad = "Educacion";
		break;
	case 6:
		this->facultad = "Humanidades";
		break;
	case 7:
		this->facultad = "Ingenieria";
		break;
	}
	aux_dia = x.Next(1, 29);
	aux_mes = x.Next(1, 13);
	aux_anio = x.Next(1980, 2005); // ya estan aprox en una universidad
	this->fecNac = aux_dia * 1000000 + aux_mes * 10000 + aux_anio;
	this->email = this->nombre + to_string(aux_anio) + "@gmail.com";
	for (int i = 0; i < 5; ++i) {
		this->redSocial[i] = x.Next(0, 2);
	}
}

