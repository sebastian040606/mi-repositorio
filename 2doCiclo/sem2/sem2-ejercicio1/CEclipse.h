#pragma once

#include <iostream>

using namespace std;
using namespace System;

class ceclipse {
private:
	char tipo;
	long fecha; //20240824
	int hora;
	bool sismo;
	bool lluvia;
	string visibilidad;
public:
	ceclipse();
	ceclipse(char tipo, long fecha, int hora, bool sismo, bool lluvias, string visibilidad);
	~ceclipse();

	char getTipo();
	long getFecha();
	int getHora();
	bool getSismo();
	bool getLLuvia();
	string getvisibilidad();

	void setTipo(char tipo);
	void setFecha(long fecha);
	void setHora(int hora);
	void setSismo(bool sismo);
	void setLluvia(bool lluvia);
	void setVisibilidad(string visibilidad);

	void generarEclipseAleatorio();
	void imprimir();
};


ceclipse::ceclipse(){
	this->tipo = ' ';
	this->fecha = 0;
	this->hora = 0;
	this->sismo = false;
	this->lluvia = false;
	this->visibilidad = "";
}
ceclipse::ceclipse(char tipo, long fecha, int hora, bool sismo, bool lluvia, string visibilidad) {
	this->tipo = tipo;
	this->fecha = fecha;
	this->hora = hora;
	this->sismo = sismo;
	this->lluvia = lluvia;
	this->visibilidad = visibilidad;
}
ceclipse::~ceclipse(){}

//getters
char ceclipse::getTipo() {
	return this->tipo;
}
long ceclipse::getFecha() {
	return this->fecha;
}
int ceclipse::getHora() {
	return this->hora;
}
bool ceclipse::getSismo() {
	return this->sismo;
}
bool ceclipse::getLLuvia() {
	return this->lluvia;
}
string ceclipse::getvisibilidad() {
	return this->visibilidad;
}

//setters
void ceclipse::setTipo(char tipo) {
	this->tipo = tipo;
}
void ceclipse::setFecha(long fecha) {
	this->fecha = fecha;
}
void ceclipse::setHora(int hora) {
	this->hora = hora;
}
void ceclipse::setSismo(bool sismo) {
	this->sismo = sismo;
}
void ceclipse::setLluvia(bool lluvia) {
	this->lluvia = lluvia;
}
void ceclipse::setVisibilidad(string visibilidad) {
	this->visibilidad = visibilidad;
}

void ceclipse::generarEclipseAleatorio() {
	Random x;
	int aux_tipo, aux_anio, aux_mes, aux_dia, aux_hora, aux_sismo, aux_lluvia, aux_visibilidad;
	long aux_fecha;

	aux_tipo = x.Next(1, 3);
	switch (aux_tipo) {
	case 1:
		this->tipo = 'S';
		break;
	case 2:
		this->tipo = 'L';
		break;
	}
	aux_anio = x.Next(2014, 2025);
	aux_mes = x.Next(1, 13);
	aux_dia = x.Next(1, 29);
	aux_fecha = aux_anio * 10000 + aux_mes * 100 + aux_dia; // ejm: 20241228
	this->fecha = aux_fecha;

	aux_hora = x.Next(1, 25);
	this->hora = aux_hora * 100; // 2400

	aux_sismo = x.Next(0, 2);
	this->sismo = aux_sismo; // 0 o 1

	aux_lluvia = x.Next(0, 2);
	this->lluvia = lluvia;

	aux_visibilidad = x.Next(1, 6);
	switch (aux_visibilidad) {
	case 1:
		this->visibilidad = "America Sur";
		break;
	case 2:
		this->visibilidad = "Europa";
		break;
	case 3:
		this->visibilidad = "Africa";
		break;
	case 4:
		this->visibilidad = "America del Norte";
		break;
	case 5:
		this->visibilidad = "Asia";
		break;
	}
}

void ceclipse::imprimir() {
	cout << "\nTipo: " << this->tipo << endl;
	cout << "fecha: " << this->fecha << endl;
	cout << "hora: " << this->hora << endl;
	cout << "sismo: " << this->sismo << endl;
	cout << "lluvia: " << this->lluvia << endl;
	cout << "visibilidad: " << this->visibilidad << endl;

}