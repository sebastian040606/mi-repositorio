#pragma once

#include <vector>
#include "CEclipse.h";

class CVectorEclipse {
private:
	int tam;
	vector<ceclipse*> *arrEclipse;
public:
	CVectorEclipse();
	~CVectorEclipse();
	int getTam();

	void agregar();
	void modificarIndex(int i);
	void eliminarIndex(int i);
	void imprimirIndex(int i);
	void imprimirTodos();
	void visibilidadEuropa();
	void reporteSismos();
	void reporteNoche();
};

CVectorEclipse::CVectorEclipse() {
	this->tam = 0;
	arrEclipse = new vector<ceclipse*>();
}
CVectorEclipse::~CVectorEclipse() {
	delete[] arrEclipse;
}
int CVectorEclipse::getTam() {
	return this->tam;
}

void CVectorEclipse::agregar() {
	ceclipse* obj = new ceclipse();
	obj->generarEclipseAleatorio();
	arrEclipse->push_back(obj);
	this->tam++;
}
void CVectorEclipse::modificarIndex(int i) {
	char aux_tipo;
	long aux_fecha;
	int aux_hora;
	bool sismo;
	bool lluvia;
	string visibilidad;
	cout << "objeto a modificar" << endl;
	this->imprimirIndex(i);
	cout << "Digite el nuevo tipo de eclipse: ";
	cin >> aux_tipo;
	cout << "Digite la nueva fecha: ";
	cin >> aux_fecha;
	cout << "Dgite la nueva hora: ";
	cin >> aux_hora;
	cout << "Digite si hay sismo: ";
	cin >> sismo;
	cout << "Digite si hay lluvia: ";
	cin >> lluvia;
	cout << "Digite la nueva visibilidad: ";
	cin >> visibilidad;

	arrEclipse->at(i)->setTipo(aux_tipo);
	arrEclipse->at(i)->setFecha(aux_fecha);
	arrEclipse->at(i)->setHora(aux_hora);
	arrEclipse->at(i)->setSismo(sismo);
	arrEclipse->at(i)->setLluvia(lluvia);
	arrEclipse->at(i)->setVisibilidad(visibilidad);
	cout << "El objeto se ha modificado correctamente";
}
void CVectorEclipse::eliminarIndex(int i) {
	cout << "Esta eliminando este objeto: " << endl;
	this->imprimirIndex(i);
	arrEclipse->erase(arrEclipse->begin() + i);
	cout << "El objeto se elimino correctamente";
	this->tam--;
}
void CVectorEclipse::imprimirIndex(int i) {
	arrEclipse->at(i)->imprimir();
}
void CVectorEclipse::imprimirTodos() {
	cout << "Los elemntos del vector son: " << endl;
	for (int n = 0; n < arrEclipse->size(); ++n) {
		arrEclipse->at(n)->imprimir();
	}
}
void CVectorEclipse::visibilidadEuropa() {
	int count = 0;
	for (int n = 0; n < arrEclipse->size(); ++n) {
		if (arrEclipse->at(n)->getvisibilidad() == "Europa") {
			this->imprimirIndex(n);
			++count;
		}
	}
	cout << "\nHubo " << count << " eclpises con visibilidad en Europa";
}
void CVectorEclipse::reporteSismos() {
	int count = 0;
	for (int n = 0; n < arrEclipse->size(); ++n) {
		if (arrEclipse->at(n)->getSismo() == true) {
			this->imprimirIndex(n);
			++count;
		}
	}
	cout << "\nHubo " << count << " eclpises con reporte de sismos";
}
void CVectorEclipse::reporteNoche() {
	int count = 0;
	for (int n = 0; n < arrEclipse->size(); ++n) {
		if ((arrEclipse->at(n)->getHora() >= 19) && (arrEclipse->at(n)->getHora() <= 24)) {
			this->imprimirIndex(n);
			++count;
		}
	}
	cout << "\nHubo " << count << " eclpises reportados de noche";
}