#include "CVectorEclipse.h"
#include<conio.h>

int main() {
	srand(int(time(0)));
	CVectorEclipse* objVectorEclipse = new CVectorEclipse();
	int opcion;
	int aux_i;
	bool flg = true;

	while (flg) {
		system("cls");
		cout << "1. Agregar aleatorio" << endl;
		cout << "2. Modificar con un indice" << endl;
		cout << "3. Eliminar con un indice" << endl;
		cout << "4. Listar por indice" << endl;
		cout << "5. Listar todo" << endl;
		cout << "6. Reporte de visibilidad en Europa" << endl;
		cout << "7. Reporte de Eclipses que ocasionaron sismo" << endl;
		cout << "8. Reporte de Eclipses de noche" << endl;
		cout << "9. Salir" << endl;
		cout << ">> ";
		cin >> opcion;

		switch (opcion) {
		case 1: system("cls");
			cout << "su objeto se creo aleatoriamente" << endl;
			objVectorEclipse->agregar();
			objVectorEclipse->imprimirIndex(objVectorEclipse->getTam() - 1);
			break;
		case 2:
			system("cls");
			cout << "Digite el indice a modificar: ";
			cin >> aux_i;
			objVectorEclipse->modificarIndex(aux_i);
			break;
		case 3:
			system("cls");
			cout << "Digite el indice a elminar: ";
			cin >> aux_i;
			objVectorEclipse->eliminarIndex(aux_i);
			break;
		case 4:
			system("cls");
			cout << "Digitre el indice a listtar: ";
			cin >> aux_i;
			objVectorEclipse->imprimirIndex(aux_i);
			break;
		case 5:
			system("cls");
			objVectorEclipse->imprimirTodos();
			break;
		case 6:
			system("cls");
			objVectorEclipse->visibilidadEuropa();
			break;
		case 7:
			system("cls");
			objVectorEclipse->reporteSismos();
			break;
		case 8:
			system("cls");
			objVectorEclipse->reporteNoche();
			break;
		case 9:
			system("cls");
			flg = false;
			cout << "Presione enter para salir...";
		}
		_getch();
	}

	return 0;
}