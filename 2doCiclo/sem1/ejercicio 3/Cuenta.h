#pragma once

#include <iostream>
#include <string>

using namespace std;

class Cuenta {
private:
	string titular;
	float cantidad;
public:
	Cuenta(string);
	Cuenta(string, float);
	void setTitular(string);
	void setCantidad(float);
	string getTitular();
	float getCantidad();
	void ingresarCantidad(double cant);
	void retirarCantidad(double cant);
};

Cuenta::Cuenta(string tit) {
	this->titular = tit;
	this->cantidad = 0;
}
Cuenta::Cuenta(string tit, float cant) {
	this->titular = tit;
	this->cantidad = cant;
}

void Cuenta::setTitular(string tit) {
	this->titular = tit;
}
void Cuenta::setCantidad(float cant) {
	this->cantidad = cant;
}
string Cuenta::getTitular() {
	return titular;
}
float Cuenta::getCantidad() {
	return cantidad;
}

void Cuenta::ingresarCantidad(double cant) {
	if (cant >= 0) {
		this->cantidad += cant;
	}
}
void Cuenta::retirarCantidad(double cant) {
	if (this->cantidad - cant < 0) {
		this->cantidad = 0;
	}
	else {
		this->cantidad = cantidad - cant;
	}
}