#include "Cuenta.h"

int main() {
	string nombre;
	cout << "Ingrese su nombre: ";
	cin >> nombre;

	Cuenta* bcp;
	bcp = new Cuenta(nombre);
	//Cantidad por defecto
	bcp->setCantidad(100);
	
	cout << "\nBienvenido al banco\n";

	while (true) {
		int option;
		double cant;
		cout << "\nSaldo restante: " << bcp->getCantidad();
		cout << "\nQue accion desea realizar?\n1.Retirar dinero\n2.Ingresar dinero\n>> ";
		cin >> option;
		switch (option){
		case 1:
			cout << "\n\nIngrese dinero a retirar: ";
			cin >> cant;
			bcp->retirarCantidad(cant);
			break;
		case 2:
			cout << "\n\nDigite el dinero ingreso: ";
			cin >> cant;
			bcp->ingresarCantidad(cant);
			break;
		}
		system("cls");
	}
	return 0;
}