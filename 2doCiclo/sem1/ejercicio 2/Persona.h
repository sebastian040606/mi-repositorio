#pragma once

#include <iostream>
#include <conio.h>
#include <string>
#include <cmath>
#include <ctime>

using namespace std;

class Persona {
private:
	string nombre;
	int edad;
	int DNI;
	char sexo;
	double peso;
	double altura;
	bool noMostrarSexo;
public:
	Persona();
	Persona(string, int, char); // nombre, edad y sexo
	Persona(string, int, int, char, double, double); // nombre, edad, dni, sexo, peso y altura
	~Persona();


	int calcIMC();
	bool esMayordeEdad();
	void comprobarSexo();
	void toString();
	void generarDNI();
	void setNombre(string);
	void setEdad(int);
	void setSexo(char);
	void setPeso(double);
	void setAltura(double);

	string getNombre();
	int getEdad();
	char getSexo();
	double getPeso();
	double getAltura();
};

Persona::Persona() {
	this->nombre = "";
	this->edad = 0;
	this->DNI = 0;
	this->sexo = 'H';
	this->peso = 0;
	this->altura = 0;
	this->noMostrarSexo = false;
}
Persona::Persona(string nom, int _edad, char _sexo) {
	this->nombre = nom;
	this->edad = _edad;
	this->sexo = _sexo;
	this->altura = 0;
	this->peso = 0;
	this->noMostrarSexo = false;
}
Persona::Persona(string nom, int _edad, int _DNI, char _sexo, double _peso, double _altura) {
	this->nombre = nom;
	this->edad = _edad;
	this->DNI = _DNI;
	this->sexo = _sexo;
	this->peso = _peso;
	this->altura = _altura;
	this->noMostrarSexo = false;
}
Persona::~Persona() {}

int Persona::calcIMC() {
	double imc = peso / (pow(altura, 2));
	if (imc < 20) {	return -1; }
	if (imc >= 20 && imc <= 25) { return 0; }
	if (imc > 25) {	return 1; }
}
bool Persona::esMayordeEdad() {
	if (edad >= 18) { return true; }
	else { return false; }
}
void Persona::comprobarSexo() {
	/*En el problema dice que si no es correcto, ademas de que el sexo quede
	por defecto, no sera visible al exterior, osea que no se podria utilizar
	el get para mostrar la variable sexo.
	*/ 
	if (sexo != 'H' && sexo != 'M') {
		sexo = 'H';
		noMostrarSexo = true;
	}
}
void Persona::toString() {
	cout << "\n\nDATOS DE LA PERSONA "<<nombre;
	cout << "\nNOMBRE: " << nombre;
	cout << "\nEDAD: " << edad;
	cout << "\nEs mayor de edad: ";
	if (esMayordeEdad()) {
		cout << "Si";
	}
	else {
		cout << "No";
	}
	cout << "\nDNI: " << DNI;
	if ((sexo == 'H' || sexo == 'M') && noMostrarSexo == false) {
		cout << "\nSEXO: " << sexo;
	}
		cout << "\nPESO: " << peso;
		cout << "\nALTURA: " << altura;
	if (peso != 0 && altura != 0) {
		cout << "\nSu IMC es: " << peso / (pow(altura, 2)) << " esta en el rango de: ";
		switch (calcIMC()) {
		case -1: cout << "debajo del peso ideal"; break;
		case 0: cout << "peso normal"; break;
		case 1: cout << "sobrepeso"; break;
		}
	}
}
void Persona::generarDNI() {
	// el tipico (maximo - minimo +1) + minimo
	DNI = rand() % (99999999 - 10000000 + 1) + 10000000;
}

void Persona::setNombre(string nom) {
	this->nombre = nom;
}
void Persona::setEdad(int _edad) {
	this->edad = _edad;
}
void Persona::setSexo(char _sexo) {
	this->sexo = _sexo;
}
void Persona::setPeso(double _peso) {
	this->peso = _peso;
}
void Persona::setAltura(double _altura) {
	this->altura = _altura;
}

string Persona::getNombre() {
	return nombre;
}
int Persona::getEdad() {
	return edad;
}
char Persona::getSexo() {
	if (noMostrarSexo = false){
		return sexo;
	}
	else {
		return 0;
	}
}
double Persona::getPeso() {
	return peso;
}
double Persona::getAltura() {
	return altura;
}