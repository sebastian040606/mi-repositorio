#include "Persona.h"

int main() {
srand(time(0));

Persona *objeto1;
objeto1 = new Persona();

string strin = "";
int num = 0;
double numdec = 0;
char carac = 0;

//Pedida de datos para el OBJETO 1
cout << "Ingrese nombre: "; cin >> strin;
objeto1->setNombre(strin);
cout << "Ingrese Edad: "; cin >> num;
objeto1->setEdad(num);
cout << "Ingrese Sexo: "; cin >> carac;
objeto1->setSexo(carac);
cout << "Ingrese su Peso en Kg: "; cin >> numdec;
objeto1->setPeso(numdec);
cout << "Ingrese su Altura en metros: "; cin >> numdec;
objeto1->setAltura(numdec);
objeto1->generarDNI();
objeto1->comprobarSexo();

//OBJETO 2

Persona* objeto2;
objeto2 = new Persona("Sebastian", 19, 'H');
objeto2->generarDNI();
objeto2->comprobarSexo();

//OBJETO 3

Persona* objeto3;
objeto3 = new Persona();
objeto3->setNombre("Juan");
objeto3->setEdad(15);
objeto3->setSexo('M');
objeto3->setAltura(1.70);
objeto3->setPeso(70);
objeto3->generarDNI();
objeto3->generarDNI();


objeto1->toString();
objeto2->toString();
objeto3->toString();
_getch();
return 0;
}