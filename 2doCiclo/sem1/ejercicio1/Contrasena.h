#pragma once

#include <iostream>
#include <conio.h>

using namespace std;

class Contrasena {
private:
	int longitud;
	string* contrasena;
public:
	Contrasena();
	Contrasena(int longitud);
	~Contrasena(){}
	int getLongitud();
	void setLongitud(int longitud);
	string getContrasena();
	void setContrasena(string* contrasena);

	bool esSeguro();
	void generarContrasena();
	void mostrarContrasena();
};
Contrasena::Contrasena(){
	this->longitud = 8;
	contrasena = new string[this->longitud];
}
Contrasena::Contrasena(int longitud){
	this->longitud = longitud;
	contrasena = new string[this->longitud];
}
int Contrasena::getLongitud() {
	return this->longitud;
}
void Contrasena::setLongitud(int longitud) {
	this->longitud = longitud;
}
string Contrasena::getContrasena() {
	return *this->contrasena;
}
void Contrasena::setContrasena(string* contrasena) {
	this->contrasena = contrasena;
}

bool Contrasena::esSeguro() {
	bool resultado = false;
	int cc_mayus, cc_minus, cc_num;
	cc_mayus = cc_minus = cc_num = 0;

	for (int i = 0; i < this->longitud; ++i) {
		if (this->contrasena[i] >= "0" && this->contrasena[i] <= "9") {
			++cc_num;
		}
		if (this->contrasena[i] >= "A" && this->contrasena[i] <= "Z") {
			++cc_mayus;
		}
		if (this->contrasena[i] >= "a" && this->contrasena[i] <= "z") {
			++cc_minus;
		}
	}
	if (cc_mayus > 2 && cc_minus && cc_num > 5) {
		resultado = true;
	}
	return resultado;
}
void Contrasena::generarContrasena() {
	int aux;
	for (int i = 0; i < this->longitud; ++i) {
		do {
			aux = rand() % (255) + 1;
		} while (!((aux >= 48 && aux <= 57) || (aux >= 65 && aux <= 90) || (aux >= 97 && aux <= 122)));
		this->contrasena[i] = char(aux);
	}

}
void Contrasena::mostrarContrasena() {
	for (int i = 0; i < this->longitud; ++i) {
		cout << this->contrasena[i];
	}
	cout << "->" << "Fuerte: (V: 1 , F: 0): " << this->esSeguro();
}