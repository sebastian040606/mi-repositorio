#include "Contrasena.h"

using namespace std;

int main(){
	srand(time(nullptr));
	int n = 10;
	int longitudAleatoria;
	Contrasena* arrContrasena = new Contrasena[n];
	for (int i = 0; i < n; ++i) {
		longitudAleatoria = rand() % (11 - 5) + 5;
		arrContrasena[i] = Contrasena(longitudAleatoria);
		arrContrasena[i].generarContrasena();
		arrContrasena[i].mostrarContrasena();
		cout << endl;
	}


	_getch();
	return 0;
}